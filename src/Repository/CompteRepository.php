<?php


namespace App\Repository;


use App\Entity\Compte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CompteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compte::class);
    }

    public function getCompteExterne()
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.interne = false')
            ->getQuery();

        return $query->getResult();
    }

    public function getCompteInterne()
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.interne = true')
            ->getQuery();

        return $query->getResult();
    }
}