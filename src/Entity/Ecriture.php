<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Ecriture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;


    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $date;

    /**
     * @ORM\ManyToOne(targetEntity="Compte", inversedBy="ecrituresDebiteur")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Compte $debiteur;

    /**
     * @ORM\ManyToOne(targetEntity="Compte", inversedBy="ecrituresCrediteur")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Compte $crediteur;

    /**
     * @ORM\Column(type="decimal", precision=2)
     */
    private float $montant;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $descriptif;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Ecriture
     */
    public function setDate(DateTime $date): Ecriture
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Compte
     */
    public function getDebiteur(): Compte
    {
        return $this->debiteur;
    }

    /**
     * @param Compte $debiteur
     * @return Ecriture
     */
    public function setDebiteur(Compte $debiteur): Ecriture
    {
        $this->debiteur = $debiteur;
        return $this;
    }

    /**
     * @return Compte
     */
    public function getCrediteur(): Compte
    {
        return $this->crediteur;
    }

    /**
     * @param Compte $crediteur
     * @return Ecriture
     */
    public function setCrediteur(Compte $crediteur): Ecriture
    {
        $this->crediteur = $crediteur;
        return $this;
    }

    /**
     * @return float
     */
    public function getMontant(): float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): Ecriture
    {
        $this->montant = $montant;
        return $this;
    }

    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    public function setDescriptif(?string $descriptif): Ecriture
    {
        $this->descriptif = $descriptif;
        return $this;
    }
}