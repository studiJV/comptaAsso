<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Compte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $nom;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $interne;

    /**
     * @ORM\OneToMany(targetEntity="Ecriture", mappedBy="debiteur")
     */
    private Collection $ecrituresDebiteur;

    /**
     * @ORM\OneToMany(targetEntity="Ecriture", mappedBy="crediteur")
     */
    private Collection $ecrituresCrediteur;

    public function getId(): int
    {
        return $this->id;
    }
    
    public function setId(int $id): Compte
    {
        $this->id = $id;
        return $this;
    }
    
    public function getNom(): string
    {
        return $this->nom;
    }
    
    public function setNom(string $nom): Compte
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getEcrituresDebiteur(): Collection
    {
        return $this->ecrituresDebiteur;
    }

    /**
     * @param Collection $ecrituresDebiteur
     * @return Compte
     */
    public function setEcrituresDebiteur(Collection $ecrituresDebiteur): Compte
    {
        $this->ecrituresDebiteur = $ecrituresDebiteur;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getEcrituresCrediteur(): Collection
    {
        return $this->ecrituresCrediteur;
    }

    /**
     * @param Collection $ecrituresCrediteur
     * @return Compte
     */
    public function setEcrituresCrediteur(Collection $ecrituresCrediteur): Compte
    {
        $this->ecrituresCrediteur = $ecrituresCrediteur;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInterne(): bool
    {
        return $this->interne;
    }

    /**
     * @param bool $interne
     * @return Compte
     */
    public function setInterne(bool $interne): Compte
    {
        $this->interne = $interne;
        return $this;
    }
}