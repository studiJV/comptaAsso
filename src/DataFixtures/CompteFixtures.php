<?php


namespace App\DataFixtures;


use App\Entity\Compte;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CompteFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i <= 10; $i++) {
            $compte = new Compte();
            $compte->setNom('un fournisseur ' . $i)
                ->setInterne(false);
            $manager->persist($compte);
        }

        for ($i = 0; $i <= 10; $i++) {
            $compte = new Compte();
            $compte->setNom('compte interne' . $i)
                ->setInterne(true);
            $manager->persist($compte);
        }

        $manager->flush();
    }
}