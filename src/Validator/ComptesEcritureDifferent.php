<?php


namespace App\Validator;


use Symfony\Component\Validator\Constraint;

class ComptesEcritureDifferent extends Constraint
{
    public $message = 'Le compte débiteur et créditeur doivent être différent';

    public function validatedBy()
    {
        return static::class.'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}