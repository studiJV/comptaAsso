<?php


namespace App\Validator;


use App\Entity\Ecriture;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ComptesEcritureDifferentValidator extends ConstraintValidator
{
    /**
     * @var Ecriture $value
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ComptesEcritureDifferent) {
            throw new UnexpectedTypeException($constraint, ComptesEcritureDifferent::class);
        }
        if ($value->getDebiteur()->getId() === $value->getCrediteur()->getId()){
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}