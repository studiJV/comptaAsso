<?php


namespace App\Controller;


use App\Action\CreerEcritureAction;
use App\Action\Entity\CreerEcriture;
use App\Action\GestionActionFactory;
use App\Form\Type\CreerEcritureType;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route(path="/ecriture", name="ecriture_")
 */
class EcritureController
{
    /**
     * @Route(path="/creer/{typeTransaction}", name="creer", condition="{'typeTransaction':'achat|vente'}")
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function creer(
        Request $request,
        Environment $twig,
        FormFactoryInterface $formFactory,
        GestionActionFactory $actionFactory,
        $typeTransaction
    ): Response
    {
        $creerEcriture = new CreerEcriture($typeTransaction);

        $form = $formFactory->createBuilder(
            CreerEcritureType::class,
            $creerEcriture
        )
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $gestionAction = $actionFactory->getCreerEcritureAction();
            $gestionAction->enregistrer($creerEcriture);
        }

        $html = $twig->render('ecriture/creer.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html);
    }
}