<?php


namespace App\Action;


use App\Action\Entity\ActionEntityInterface;

interface ActionInterface
{
    public function valider(ActionEntityInterface $data): bool;
}