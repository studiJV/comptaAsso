<?php


namespace App\Action\Entity;


use App\Entity\Compte;
use App\Entity\Ecriture;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AppAssert;

/**
 * @AppAssert\ComptesEcritureDifferent()
 */
class CreerEcriture implements ActionEntityInterface
{
    /**
     * @Assert\Type("\DateTime")
     * @Assert\NotNull()
     */
    private DateTime $date;

    /**
     * @Assert\NotNull()
     * @Assert\Type("App\Entity\Compte")
     */
    private Compte $debiteur;

    /**
     * @Assert\NotNull()
     * @Assert\Type("App\Entity\Compte")
     */
    private Compte $crediteur;

    /**
     * @Assert\PositiveOrZero()
     */
    private float $montant;

    private ?string $descriptif = null;

    private string $typeEcriture;

    /**
     * CreerEcriture constructor.
     * @param string $typeEcriture
     */
    public function __construct(string $typeEcriture)
    {
        $this->typeEcriture = $typeEcriture;
    }


    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): CreerEcriture
    {
        $this->date = $date;
        return $this;
    }

    public function getDebiteur(): Compte
    {
        return $this->debiteur;
    }

    public function setDebiteur(Compte $debiteur): CreerEcriture
    {
        $this->debiteur = $debiteur;
        return $this;
    }

    public function getCrediteur(): Compte
    {
        return $this->crediteur;
    }

    public function setCrediteur(Compte $crediteur): CreerEcriture
    {
        $this->crediteur = $crediteur;
        return $this;
    }

    public function getMontant(): float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): CreerEcriture
    {
        $this->montant = $montant;
        return $this;
    }

    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    public function setDescriptif(?string $descriptif): CreerEcriture
    {
        $this->descriptif = $descriptif;
        return $this;
    }

    public function toDoctrineEntity(): Ecriture
    {
        return (new Ecriture())
            ->setCrediteur($this->getCrediteur())
            ->setDate($this->getDate())
            ->setDebiteur($this->getDebiteur())
            ->setDescriptif($this->getDescriptif())
            ->setMontant($this->getMontant());
    }

    public function getTypeEcriture(): string
    {
        return $this->typeEcriture;
    }
}