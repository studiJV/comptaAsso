<?php


namespace App\Action\Entity;


interface ActionEntityInterface
{
    public function toDoctrineEntity();
}