<?php


namespace App\Action;


use App\Action\Entity\CreerEcriture;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GestionActionFactory
{
    private GestionAction $gestionAction;

    public function __construct(GestionAction $gestionAction)
    {
        $this->gestionAction = $gestionAction;
    }

    public function getCreerEcritureAction(): GestionAction
    {
        $this->gestionAction->setEntityClassName(CreerEcriture::class);
        return $this->gestionAction;
    }
}