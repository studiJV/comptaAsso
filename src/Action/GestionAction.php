<?php


namespace App\Action;


use App\Action\Entity\ActionEntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;

class GestionAction implements ActionInterface
{
    private ValidatorInterface $validator;
    private EntityManagerInterface $entityManager;

    private ConstraintViolationListInterface $error;

    private string $entityClassName;

    public function __construct(
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    )
    {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function enregistrer(ActionEntityInterface $data):void
    {
        if($this->valider($data)) {
            $this->entityManager->persist($data->toDoctrineEntity());
            $this->entityManager->flush();
        }
    }

    public function valider(ActionEntityInterface $data): bool
    {
        if ($data instanceof $this->entityClassName) {
            $this->error = $this->validator->validate($data);
            return count($this->error) === 0;
        }
        return false;
    }

    public function setEntityClassName(string $entityClassName): GestionAction
    {
        $this->entityClassName = $entityClassName;
        return $this;
    }
}