<?php


namespace App\Form\Type;


use App\Action\Entity\CreerEcriture;
use App\Entity\Compte;
use App\Repository\CompteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreerEcritureType extends AbstractType
{
    private CompteRepository $compteRepository;

    public function __construct(
        CompteRepository $compteRepository
    )
    {
        $this->compteRepository = $compteRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($builder->getData()->getTypeEcriture() === 'achat') {
            $comptesDebiteur = $this->compteRepository->getCompteInterne();
            $comptesCrediteur = $this->compteRepository->getCompteExterne();
        } else {
            $comptesDebiteur = $this->compteRepository->getCompteExterne();
            $comptesCrediteur = $this->compteRepository->getCompteInterne();
        }

        $builder
            ->add('date', DateType::class, [
                'widget'=>'single_text'
            ])
            ->add('debiteur', EntityType::class, [
                'class' => Compte::class,
                'choice_label' => 'nom',
                'choices' => $comptesDebiteur
            ])
            ->add('crediteur', EntityType::class, [
                'class' => Compte::class,
                'choice_label' => 'nom',
                'choices' => $comptesCrediteur
            ])
            ->add('descriptif', TextareaType::class, [
                'required' => false
            ])
            ->add('montant', NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreerEcriture::class,
        ]);
    }
}